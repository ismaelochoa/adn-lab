import requests

def test_get_check_api():
     response = requests.get("https://hxe4xsa80i.execute-api.us-east-1.amazonaws.com/mutant")
     assert response.status_code == 200

def test_is_mutant():
     response = requests.post("https://hxe4xsa80i.execute-api.us-east-1.amazonaws.com/mutant",  json= {"dna":["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]})
     assert response.status_code == 200

def test_is_not_mutant():
     response = requests.post("https://hxe4xsa80i.execute-api.us-east-1.amazonaws.com/mutant",  json= {"dna":["ATTTCA","CCGTGC","TCATGG","AGAAGG","ACCGTA","TCTCTG"]})
     assert response.status_code == 403