import sys
import logging
import os
import pymysql
import json
import uuid
from datetime import datetime
from .services.lab_class import LabClass

#Coneccion a BD en RDS
dbhost  = os.environ.get('db_host')
dbuser = os.environ.get('db_username')
dbpassword = os.environ.get('db_password')
dbname = os.environ.get('db_name')
dbport = 3306

logger = logging.getLogger()
logger.setLevel(logging.INFO)

now = datetime.now()
formatted_date = now.strftime('%Y-%m-%d %H:%M:%S')

try:
    conn = pymysql.connect(
        host=dbhost,
        user=dbuser, 
        password = dbpassword,
        db=dbname,
        port=dbport
        )
except pymysql.MySQLError as e:
    logger.error("ERROR: no se puede conectar a MySQL.")
    logger.error(e)
    sys.exit()

logger.info("Exito: coneccion a MySQL exitosa")

GET_RAW_PATH = "/stats"
CREATE_RAW_PATH = "/mutation"

def lambda_handler(event, context):
    print(event)
    labcn = conn.cursor ()
    if event['rawPath'] == GET_RAW_PATH:
        print('recibe stats request')
        labcn.execute('SELECT COUNT(case when ismutante=1 then 1 end) as totalmutante, COUNT(case when ismutante=0 then 1 end) as totalnomutante, COUNT(case when ismutante=1 then 1 end) / COUNT(case when ismutante=0 then 1 end) as rating FROM adnregistro.mutante')
        rows = labcn.fetchall()
        for row in rows:
            return { "count_mutations": row[0], "count_no_mutation": row[1], "ratio": row[2] }
        return { "count_mutations": 0, "count_no_mutation": 0, "ratio": 0 }

    elif event['rawPath'] == CREATE_RAW_PATH:
        print('recibe mutation request')
        decodedBody = json.loads(event['body'])
        dna = decodedBody['dna']
        ms = LabClass()
        if not ms.hasMutation(dna):
            labcn.execute("INSERT INTO mutante (ismutante, dna, created_at) VALUES (%s, %s, %s)", (0, str(dna), formatted_date))
            return {
            'statusCode': 403,
            'body': json.dumps("hasMutation: FALSE " + str(dna))
            }
        labcn.execute("INSERT INTO mutante (ismutante, dna, created_at) VALUES (%s, %s, %s)", (1, str(dna), formatted_date))
        return {
            'statusCode': 200,
            'body': json.dumps("hasMutation: TRUE " + str(dna))
        }
