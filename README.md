# LabADN

### Desafío Sr. Full Stack Developer:

proyecto que detecte si una persona tiene diferencias genéticas basándose en su secuencia de ADN

## Objetivo

Desarrollar una aplicacion que determine sobre una cadena de ADN recibida si esta tiene caracteristicas mutantes al contener 4 letras iguale [AAAA], [CCCC], etc.

## Desafío 1

se creo una classe *hasMutation* lacual revisa la secuencia del ADN obtenido de forma Vertical, Horizontal y Oblicua

```
services/lab_class.py
```

## Desafío 2

Crear una API REST, hostear esa API en un cloud.

Paa este caso utilizamos los servicios de AWS:

- [Amazon API Gateway](https://aws.amazon.com/es/api-gateway/)
- [AWS Lambda](https://aws.amazon.com/es/lambda/)

Donde se cargo el codigo para analizar los patrones del ADN que se recibiria atravez de [POST /mutation](https://hxe4xsa80i.execute-api.us-east-1.amazonaws.com/mutation)
En caso de verificar una mutación, debería devolver un 200, en caso contrario un 403.

## Desafío 3

Se creo tambien una seccion de respuesta para mostrar las estadisticas de los registros procesados atravez de [GET /stats](https://hxe4xsa80i.execute-api.us-east-1.amazonaws.com/stats)

Con la ayuda de *Amazon API Gateway* la cual esta configirada para servir el 🚀 [API](https://hxe4xsa80i.execute-api.us-east-1.amazonaws.com) donde se controla aspecto de seguridad y fluctuaciones agresivas de tráfico.

Para el almacenamiento de la informacion se utilizo el Servicio de [RDS](https://aws.amazon.com/es/rds/mariadb/) en su version MariDB, con la siguiente estructura:

```

-- Dumping database structure for adnregistro
CREATE DATABASE IF NOT EXISTS `adnregistro` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `adnregistro`;

-- Dumping structure for table adnregistro.mutante
CREATE TABLE IF NOT EXISTS `mutante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ismutante` blob NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `dna` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

```

Los datos de accesos para la base de datos se almacenaro en las Variables de entorno de *Lambda* para un mejor nivel de seguridad.

## Test

Se genero un 🤖 Test *test/test_api.py* para verificar el correcto funcionamiento.

Use the built-in continuous integration in GitLab.

## URL API

[GET /stats](https://hxe4xsa80i.execute-api.us-east-1.amazonaws.com/stats) ->  https://hxe4xsa80i.execute-api.us-east-1.amazonaws.com/stats

[POST /mutation](https://hxe4xsa80i.execute-api.us-east-1.amazonaws.com/mutation) -> https://hxe4xsa80i.execute-api.us-east-1.amazonaws.com/mutation


```
POST /mutation
{
"dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}
```

Se recomienda utilizar la aplicacion  [postman - test api](https://www.postman.com/api-platform/api-testing/) para una manera mas sencilla de hacer pruebas con el API.

## Happy Coding  😃